const buttonWrapper = document.querySelector('.btn-wrapper');
const buttonArray = [...buttonWrapper.children].map(element => {
    return element.textContent.toLowerCase()
});

let previosKey;
const previosKeyColor = '#000000'
const activeKeyColor = 'blue'
document.addEventListener('keyup', function (event) {
    activeKey = event.key.toLowerCase()
    if (keyCheck(activeKey, buttonArray)) {
        if (activeKey !== previosKey) {
            changeColor(buttonArray.indexOf(activeKey), activeKeyColor)
            changeColor(buttonArray.indexOf(previosKey), previosKeyColor)
            previosKey = activeKey
        }
    }
})
function changeColor(changeEl, colorEl) {
    if (changeEl >= 0) {
        console.log(changeEl);
        buttonWrapper.children[changeEl].style.backgroundColor = colorEl //`${colorEl}`
    }
}
function keyCheck(activeKey, defaultKeys) {
    if (defaultKeys.includes(activeKey)) {
        return true
    }
    return false
}
